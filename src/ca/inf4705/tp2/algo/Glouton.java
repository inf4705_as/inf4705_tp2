package ca.inf4705.tp2.algo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import ca.inf4705.tp2.utils.ReadingPR;
import ca.inf4705.tp2.utils.ReadedData;

public class Glouton {

    public static ArrayList<Integer> getTheCombinaison(int weight, Integer[] data) {

        ArrayList<Integer> solution = new ArrayList<Integer>(2);

        while (!is_done(data) && !isSolution(weight, data)) {
            int x_index = choice(data);
            int max_weight = data[x_index];

            if (legal(weight, max_weight)) {
                weight -= max_weight;
                data[x_index] = -1;
                solution.add(max_weight);
            } else {
                data[x_index] = -data[x_index];
            }
        }

        if (isSolution(weight, data)) {
            return solution;
        } else {
            System.out.println("no solution founded");
            return null;
        }
    }

    private static boolean isSolution(int weight, Integer[] arrayOfSticks) {
        if (weight < 0) {
            System.err.println("Il y a eu un probleme dans l'algo");
        }
        return (weight == 0 || get_min_value(arrayOfSticks) > weight);
    }

    private static int get_min_value(Integer[] arrayOfSticks) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < arrayOfSticks.length; i++) {
            if (arrayOfSticks[i] > 0 && arrayOfSticks[i] < min) {
                min = arrayOfSticks[i];
            }
        }
        return min;
    }

    private static int choice(Integer[] arrayOfSticks) {
        int index_max = 0;
        int max = arrayOfSticks[0];
        for (int i = 1; i < arrayOfSticks.length; i++) {
            if (arrayOfSticks[i] > max) {
                max = arrayOfSticks[i];
                index_max = i;
            }
        }
        return index_max;
    }

    private static boolean is_done(Integer[] arrayOfSticks) {
        int index = choice(arrayOfSticks);
        int max_val = arrayOfSticks[index];
        return max_val == -1;
    }

    private static boolean legal(int weight, int max_weight_stick) {
        return weight >= max_weight_stick;
    }

    public static void main(String[] args) {

        boolean isPrint = false;
        boolean isPath = false;
        String e_path = "";
        boolean isTime = false;

        if (args != null && args.length == 4) {
            isPrint = Boolean.valueOf(args[0]);
            isPath = Boolean.valueOf(args[1]);
            e_path = args[2];
            isTime = Boolean.valueOf(args[3]);
        }

        ReadingPR rpr = new ReadingPR();
        ReadedData data = null;
        try {
            data = rpr.read_func(e_path, isPath);
        } catch (IOException IOE) {
            System.err.println("");
        }
        if (data != null) {
            double start_time = (double) System.nanoTime();
            ArrayList<Integer> listOfSolutions = Glouton.getTheCombinaison(data.getMaxWeight(),
                    data.getArrayOfSticks());
            double end_time = System.nanoTime();
            double total_time = (end_time - start_time) / 1e6;
            if (isPrint) {
                System.out.println("" + listOfSolutions.toString());
            }
            if (isTime) {
                System.out.println("" + total_time);
            }
        }
    }

}
