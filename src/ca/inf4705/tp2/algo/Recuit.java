package ca.inf4705.tp2.algo;

import java.io.IOException;
import ca.inf4705.tp2.utils.ReadingPR;
import ca.inf4705.tp2.utils.Voisin;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import ca.inf4705.tp2.utils.*;
import java.util.Random;

import ca.inf4705.tp2.utils.ReadedData;

public class Recuit {

    public ArrayList<Integer> recuit(ArrayList<Integer> solutionInitial, ReadedData data) {
        // essaie 5:(K = 10,P =20, T=40 , Alpha =0.4 )
        int kmax = 10;
        int p = 20;
        float alpha = 0.4f;
        float teta = 40f;
        int Dif;
        ArrayList<Integer> actualSolution = new ArrayList<Integer>(solutionInitial);
        ArrayList<Integer> bestSolution = new ArrayList<Integer>(actualSolution);
        ArrayList<Integer> restOfSticks = new ArrayList<Integer>(Arrays.asList(data.getArrayOfSticks()));
        Voisin newVoisin = new Voisin();
        Voisin tempVoisin = new Voisin();

        for (int i = restOfSticks.size() - 1; i >= 0; i--) {
            if (restOfSticks.get(i) == -1) {
                restOfSticks.remove(i);
            } else if (restOfSticks.get(i) < 0) {
                restOfSticks.set(i, (restOfSticks.get(i) * -1));
            }
        }

        tempVoisin.setNumberOfSticksRestant(new ArrayList<Integer>(restOfSticks));
        tempVoisin.setVoisin(new ArrayList<Integer>(solutionInitial));
        for (int i = 0; i < kmax; i++) {
            for (int j = 0; j < p; j++) {
                int sum_solution = somme(actualSolution);

                newVoisin = voisin(tempVoisin, data.getMaxWeight());
                int sum_newvoisin = somme(newVoisin.getVoisin());

                sum_solution = somme(actualSolution);

                Dif = sum_newvoisin - sum_solution;
                if (Dif >= 0 || calcul(Dif, teta)) {
                    actualSolution = newVoisin.getVoisin();
                    int sum_best = somme(bestSolution);

                    if (somme(actualSolution) > sum_best) {
                        bestSolution = actualSolution;

                    }
                }
                tempVoisin.setVoisin(new ArrayList<Integer>(actualSolution));
                tempVoisin.setNumberOfSticksRestant(new ArrayList<Integer>(newVoisin.getNumberOfSticksRestant()));
            }
            teta = teta * alpha;
        }

        return (bestSolution);
    }

    public static int somme(ArrayList<Integer> array) {
        int i;
        int sum = 0;
        for (i = 0; i < array.size(); i++)
            sum += array.get(i);
        return sum;
    }

    public Voisin voisin(Voisin voisin, int maximalWeight) {
        ArrayList<Integer> arrayOfSticksRestant = new ArrayList<Integer>(voisin.getNumberOfSticksRestant());
        ArrayList<Integer> voisinTemp = new ArrayList<Integer>(voisin.getVoisin());

        int index = new Random().nextInt(arrayOfSticksRestant.size());
        Integer newWeight = arrayOfSticksRestant.get(index);
        voisinTemp.add(newWeight);
        arrayOfSticksRestant.remove(newWeight);
        int sum_array = somme(voisinTemp);

        if (sum_array > maximalWeight) {
            int indexTemp;
            do {
                indexTemp = new Random().nextInt(voisinTemp.size());
                arrayOfSticksRestant.add(voisinTemp.get(indexTemp));
                voisinTemp.remove(indexTemp);
                sum_array = somme(voisinTemp);
            } while (somme(voisinTemp) > maximalWeight);
        }
        voisin.setVoisin(voisinTemp);
        voisin.setNumberOfSticksRestant(arrayOfSticksRestant);
        return voisin;
    }

    public boolean calcul(int dif, float teta) {
        float temp = 0;

        temp = dif / teta;
        return Math.exp(temp) >= unif(0, 1);

    }

    public float unif(int a, int b) {
        Random r = new Random();
        float valRand = (r.nextFloat() * (b - a)) + a;
        return valRand;
    }

    public static void main(String[] args) {

        boolean isPrint = false;
        boolean isPath = false;
        String e_path = "";
        boolean isTime = false;

        if (args != null && args.length == 4) {
            isPrint = Boolean.valueOf(args[0]);
            isPath = Boolean.valueOf(args[1]);
            e_path = args[2];
            isTime = Boolean.valueOf(args[3]);
        }

        ReadingPR rpr = new ReadingPR();
        ReadedData data = null;
        try {
            data = rpr.read_func(e_path, isPath);

        } catch (IOException IOE) {
            System.err.println("");
        }
        Recuit rec = new Recuit();
        ArrayList<Integer> Sol = Glouton.getTheCombinaison(data.getMaxWeight(), data.getArrayOfSticks());
        double start_time = (double) System.nanoTime();
        ArrayList<Integer> listOfSolutions = rec.recuit(Sol, data);
        double end_time = System.nanoTime();

        double total_time = (end_time - start_time) / 1e6;
        if (isPrint) {
            System.out.println("" + listOfSolutions.toString());
        }
        if (isTime) {
            System.out.println("" + total_time);
        }
    }
}