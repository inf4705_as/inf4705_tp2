package ca.inf4705.tp2.algo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.*;
import java.util.HashSet;
import java.util.Arrays;
import ca.inf4705.tp2.utils.*;
import ca.inf4705.tp2.utils.ReadingPR;
import ca.inf4705.tp2.utils.ReadedData;
import ca.inf4705.tp2.utils.ObjectDoubleTableau;
import ca.inf4705.tp2.algo.*;

public class DynamiqueEssai1 {

    private static Integer ZERO = 0;

    private ObjectDoubleTableau faire_monnaie(int N, int n, ArrayList<Integer> dn) {
        // définir c[1 . . .N] et I[1 . . .N] ;
        int[] C = new int[N + 1];
        int[] I = new int[N + 1];
        C[0] = 0;
        I[0] = 0;

        // pour j = 1 à N faire
        for (int j = 1; j <= N; ++j) {
            // c[j] ← ∞;
            C[j] = ZERO;
        }

        // pour i = 1 à n faire
        for (int i = 1; i <= n; ++i) {
            // c[di] ← 1 ;
            C[dn.get(i - 1)] = dn.get(i - 1);
            // I[di] ← −i ;
            I[dn.get(i - 1)] = -dn.get(i - 1);// -1;
        }

        // Reference :
        // https://www.geeksforgeeks.org/find-the-largest-pair-sum-in-an-unsorted-array/
        // pour j = 1 à N faire
        for (int j = 1; j <= N; ++j) {
            if (C[j] == ZERO) {
                int[] temp = new int[j];
                for (int k = 0; k < j; ++k) {
                    temp[k] = C[k];
                }
                Stack<Integer> st = DynamiqueEssai1.printClosest(temp, temp.length, j);
                int i1 = st.pop();
                int i2 = st.pop();
                C[j] = i1 + i2;
                if (i2 == ZERO)
                    I[j] = i1;
                else
                    I[j] = i2;

            }
        }
        ObjectDoubleTableau doubleTab = new ObjectDoubleTableau(C, I);
        // retourner c[N] et I ;
        return doubleTab;
    }

    public ArrayList<Integer> getResultTable(ArrayList<Integer> dn, int[] I, int index) {
        ArrayList<Integer> tableResult = new ArrayList<Integer>();

        int what_is_left = index;

        if (I[index] < 0) {
            what_is_left = 0;
            tableResult.add(index);
        }

        while (what_is_left > 0) {
            ArrayList<Integer> al = new ArrayList<>();
            if (Arrays.asList(dn).indexOf(I[index]) < 0) {
                al = getResultTable(dn, I, I[index]);
                tableResult.addAll(al);
            } else
                tableResult.add(I[index]);
            what_is_left = index = what_is_left - I[index];
            if (I[index] <= 0) {
                tableResult.add(index);
                break;
            }
        }
        return tableResult;
    }

    /**
     * @see https://stackoverflow.com/questions/8892360/convert-set-to-list-without-creating-new-list
     * @param dataSet
     * @return
     */
    private ArrayList<Integer> convertSetToArrayList(Set<Integer> dataSet) {
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.addAll(dataSet);
        return array;
    }

    private Set<Integer> convertArrayToSet(int[] dataArray) {
        Set<Integer> mySet = new HashSet<Integer>();
        for (int varInt : dataArray) {
            mySet.add(varInt);
        }
        return mySet;
    }

    private Set<Integer> convertArrayToSet(Integer[] dataArray) {
        Set<Integer> mySet = new HashSet<Integer>();
        for (int varInt : dataArray) {
            mySet.add(varInt);
        }
        return mySet;
    }

    public static void main(String[] args) {
        boolean isPrint = false;
        boolean isPath = false;
        String e_path = "";
        boolean isTime = false;

        if (args != null && args.length == 4) {
            isPrint = Boolean.valueOf(args[0]);
            isPath = Boolean.valueOf(args[1]);
            e_path = args[2];
            isTime = Boolean.valueOf(args[3]);

        }

        ReadingPR rpr = new ReadingPR();
        ReadedData data = null;
        DynamiqueEssai1 dyna1 = new DynamiqueEssai1();
        ObjectDoubleTableau doubleTab = null;
        try {
            data = rpr.read_func(e_path, isPath);
        } catch (IOException IOE) {
            System.err.println("file : " + e_path);
        }
        if (data != null) {
            ArrayList<Integer> dn = dyna1.convertSetToArrayList(dyna1.convertArrayToSet(data.getArrayOfSticks()));
            int n = dn.size();
            int N = data.getMaxWeight();
            Collections.sort(dn);

            double start_time = (double) System.nanoTime();
            doubleTab = dyna1.faire_monnaie(N, n, dn);

            // Source :
            // https://stackoverflow.com/questions/9707938/calculating-time-difference-in-milliseconds
            ArrayList<Integer> listOfSolutions = dyna1.getResultTable(dn, doubleTab.getI(), data.getMaxWeight());
            double end_time = System.nanoTime();
            double total_time = (end_time - start_time) / 1e6;
            if (isPrint) {
                System.out.println("" + listOfSolutions.toString());
            }
            if (isTime) {
                System.out.println("" + total_time);
            }
        }
    }

    // Prints the pair with sum cloest to x
    // Source :
    // https://www.geeksforgeeks.org/given-sorted-array-number-x-find-pair-array-whose-sum-closest-x/?fbclid=IwAR2EgrBLN35SNroKJJ2swKO_yd_Wi14McoE5rK9Y7oZuYuW3Gpn9HMyGNdk
    static Stack<Integer> printClosest(int arr[], int n, int x) {
        int res_l = 0, res_r = 0; // To store indexes of result pair

        // Initialize left and right indexes and difference between
        // pair sum and x
        int l = 0, r = n - 1, diff = Integer.MAX_VALUE;

        // While there are elements between l and r
        while (r > l) {
            // Check if this pair is closer than the closest pair so far
            if (Math.abs(arr[l] + arr[r] - x) <= diff) {
                res_l = l;
                res_r = r;
                diff = Math.abs(arr[l] + arr[r] - x);
            }

            // If this pair has more sum, move to smaller values.
            if (arr[l] + arr[r] > x)
                r--;
            else // Move to larger values
                l++;
        }
        Stack<Integer> st = new Stack<Integer>();
        st.push(arr[res_l]);
        st.push(arr[res_r]);
        return st;
    }

}