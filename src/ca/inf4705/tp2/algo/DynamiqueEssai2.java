package ca.inf4705.tp2.algo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.*;
import java.util.HashSet;
import java.util.Arrays;
import ca.inf4705.tp2.utils.*;
import ca.inf4705.tp2.utils.ReadingPR;
import ca.inf4705.tp2.utils.ReadedData;
import ca.inf4705.tp2.utils.ObjectDoubleTableau;

public class DynamiqueEssai2 {

    public class Obj {
        private Integer weight = 0;
        private Integer numberOfStick = 0;
        private ArrayList<Integer> arrayOfWeight = null;

        public Obj(Integer w, Integer numberOfStick, ArrayList<Integer> arrayOfWeight) {
            this.weight = w;
            this.numberOfStick = numberOfStick;
            this.arrayOfWeight = arrayOfWeight;
        }

        public String toString() {
            return "\ntype batton : " + this.weight + "\t" + "nombre : " + this.numberOfStick + "\t"
                    + "arrayOfWeight : " + arrayOfWeight.toString();
        }
    }

    private ArrayList<Obj> faire_monnaie(int N, int n, HashMap<Integer, Integer> dn) {
        ArrayList<Obj> tableau_essaie_2 = new ArrayList<>(n);
        // Etape 1 : On initialise le tableau avec des valeurs juste a 0 :
        for (Integer key : dn.keySet()) {
            ArrayList<Integer> arrayOfWeight = new ArrayList<>(N + 1);
            for (int i = 0; i <= N; ++i) {
                arrayOfWeight.add(0);
            }
            Obj value = new Obj(key, dn.get(key), arrayOfWeight);
            tableau_essaie_2.add(value);
        }

        // Etape 2 : Definition de la recurrence voir dans l'etape 4:

        // Etape 3 : Valeurs frontiere
        for (Obj obj : tableau_essaie_2) {
            if (obj.arrayOfWeight.size() >= obj.weight) {
                obj.arrayOfWeight.set(obj.weight, obj.weight);
                obj.numberOfStick -= 1;
            }
        }

        // Etape 4 : Remplissage du tableau selon la recurrence.

        for (int i = 0; i < tableau_essaie_2.size(); i++) {
            Obj obj = tableau_essaie_2.get(i);
            for (int j = 1; j < obj.arrayOfWeight.size(); j++) {
                if (obj.weight != j) {
                    Integer valeur_gauche = null;
                    Integer valeur_haut = null;
                    if ((j - obj.weight) >= 0) {
                        valeur_gauche = 1 + obj.arrayOfWeight.get(j - obj.weight);
                    }
                    if (i - 1 >= 0) {
                        valeur_haut = tableau_essaie_2.get(i - 1).arrayOfWeight.get(j);
                    }
                    Integer min_valeur = null;
                    if (valeur_gauche != null && valeur_haut != null) {
                        min_valeur = Math.min(valeur_gauche, valeur_haut);
                    } else if (valeur_gauche != null && valeur_haut == null) {
                        min_valeur = valeur_gauche;
                    } else if (valeur_gauche == null && valeur_haut != null) {
                        min_valeur = valeur_haut;
                    } else {
                        min_valeur = 0;
                    }
                    tableau_essaie_2.get(i).arrayOfWeight.set(j, min_valeur);
                }
            }
            tableau_essaie_2.set(i, obj);
        }

        return tableau_essaie_2;
    }

    // Etape 5 : Recuperation de la reponse :
    private ArrayList<Integer> getSolution(Integer N, Integer index, ArrayList<Obj> table_essaie_2) {

        ArrayList<Integer> Result = new ArrayList<>();
        Integer w = table_essaie_2.get(index).weight;
        Integer left_index = N - w;
        Integer upper_index = index - 1;

        Integer left_value = Integer.MAX_VALUE;
        Integer upper_value = Integer.MAX_VALUE;

        if (left_index >= 0) {
            left_value = table_essaie_2.get(index).arrayOfWeight.get(left_index) + 1;
        }

        if (upper_index >= 0) {
            upper_value = table_essaie_2.get(upper_index).arrayOfWeight.get(N);
        }

        if (N <= 0) {
            return Result;
        }
        if (index == 0) {
            Result.add(w);
            return Result;
        }

        if (Math.min(left_value, upper_value) == upper_value) {
            index--;
            Result.addAll(getSolution(N, index, table_essaie_2));
        } else {
            Result.addAll(getSolution(N - w, index, table_essaie_2));
            Result.add(w);
        }
        return Result;
    }

    public static void main(String[] args) {

        boolean isPrint = false;
        boolean isPath = false;
        String e_path = "";
        boolean isTime = false;

        if (args != null && args.length == 4) {
            isPrint = Boolean.valueOf(args[0]);
            isPath = Boolean.valueOf(args[1]);
            e_path = args[2];
            isTime = Boolean.valueOf(args[3]);

        }

        ReadingPR rpr = new ReadingPR();
        ReadedData data = null;
        DynamiqueEssai2 dyna2 = new DynamiqueEssai2();
        try {
            data = rpr.read_func(e_path, isPath);
        } catch (IOException IOE) {
            System.err.println("");
        }
        if (data != null) {
            int n = 0;
            int N = data.getMaxWeight();
            HashMap<Integer, Integer> dn = new HashMap<>();

            for (int i = 0; i < data.getArrayOfSticks().length; ++i) {
                int element = data.getArrayOfSticks()[i];
                if (dn.keySet().contains(element)) {
                    dn.put(element, (dn.get(element) + 1));
                } else {
                    dn.put(element, 1);
                }
            }
            n = dn.keySet().size();

            double start_time = (double) System.nanoTime();
            ArrayList<Obj> table_essaie_2 = dyna2.faire_monnaie(N, n, dn);

            ArrayList<Integer> listOfSolutions = dyna2.getSolution(N, (dn.size() - 1), table_essaie_2);

            double end_time = System.nanoTime();
            double total_time = (end_time - start_time) / 1e6;

            if (isPrint) {
                System.out.println("" + listOfSolutions.toString());
            }
            if (isTime) {
                System.out.println("" + total_time);
            }

        }
    }

}