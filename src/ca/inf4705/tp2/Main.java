package ca.inf4705.tp2;
import java.io.IOException;
import ca.inf4705.tp2.utils.ReadingPR;
import ca.inf4705.tp2.utils.ReadedData;

public class Main{
    public static void main(String[] args){
        ReadingPR rpr = new ReadingPR();
        ReadedData data = null;
        try{
            data = rpr.read_func(100, 400, 1);
        }catch(IOException IOE){
            System.err.println("");
        }
    
        rpr.println(data.getArrayOfSticks());
        System.out.println(data.toString());
    }
}