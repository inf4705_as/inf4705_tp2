package ca.inf4705.tp2.utils;

import ca.inf4705.tp2.utils.*;
import java.util.ArrayList;

public class Voisin {

    private ArrayList<Integer> voisin;
    private ArrayList<Integer> numberOfSticksRestant;

    public Voisin() {
    }

    public void setVoisin(ArrayList<Integer> newVoisin) {
        this.voisin = newVoisin;
    }

    public void setNumberOfSticksRestant(ArrayList<Integer> newNumber) {
        this.numberOfSticksRestant = newNumber;
    }

    public ArrayList<Integer> getVoisin() {
        return voisin;
    }

    public ArrayList<Integer> getNumberOfSticksRestant() {
        return numberOfSticksRestant;
    }
}