package ca.inf4705.tp2.utils;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

import ca.inf4705.tp2.utils.*;
import ca.inf4705.tp2.utils.ReadedData;

public class ReadingPR {
    public ReadingPR() {
    }

    public void println(int[] array) {

        for (int i = 0; i < array.length; ++i) {
            System.out.println(array[i]);
        }
    }

    public void println(Integer[] array) {

        for (int i = 0; i < array.length; ++i) {
            System.out.println(array[i]);
        }
    }

    static String folder_verification = System.getProperty("user.dir") + "/verification/";
    static String folder = System.getProperty("user.dir") + "/src/ressources/jeux_de_donnees/";

    public ReadedData read_func(int numberOfSticks, int weight, int copyNumber) throws IOException {
        Integer[] array = null;
        String fileName = "PR_" + numberOfSticks + "_" + weight + "_" + copyNumber + ".txt";
        File file = new File(ReadingPR.folder + fileName);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        if ((line = bufferedReader.readLine()) != null) {
            numberOfSticks = Integer.parseInt(line);
            array = new Integer[numberOfSticks];// [2];
            int index = 0;
            while (index < numberOfSticks && (line = bufferedReader.readLine()) != null) {
                String[] sT = line.split("\t");
                array[index] = Integer.parseInt(sT[1]);
                ++index;
            }
        }
        int maxWeight = 0;
        if ((line = bufferedReader.readLine()) != null) {
            maxWeight = Integer.parseInt(line);
        }

        ReadedData data = new ReadedData(numberOfSticks, array, maxWeight);

        return data;
    }

    public ReadedData read_verification(int num_fichier, int numberOfSticks) throws IOException {
        Integer[] array = null;
        String fileName = "exemplaire" + num_fichier + ".txt";
        File file = new File(ReadingPR.folder_verification + fileName);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        if ((line = bufferedReader.readLine()) != null) {
            numberOfSticks = Integer.parseInt(line);
            array = new Integer[numberOfSticks];// [2];
            int index = 0;
            while (index < numberOfSticks && (line = bufferedReader.readLine()) != null) {
                String[] sT = line.split("\t");
                array[index] = Integer.parseInt(sT[1]);
                ++index;
            }
        }
        int maxWeight = 0;
        if ((line = bufferedReader.readLine()) != null) {
            maxWeight = Integer.parseInt(line);
        }

        ReadedData data = new ReadedData(numberOfSticks, array, maxWeight);

        return data;
    }

    public ReadedData read_func(String fileName_input, boolean isAbsolue) throws IOException {
        String[] input;
        if (!isAbsolue) {
            if (fileName_input.contains("PR"))
                input = fileName_input.split("_");
            else {
                input = fileName_input.split("-");
            }
        } else {
            String[] file_temp = fileName_input.split("/");
            if (file_temp[file_temp.length - 1].contains("PR"))
                input = file_temp[file_temp.length - 1].split("_");
            else {
                input = file_temp[file_temp.length - 1].split("-");
            }
        }

        String fileType = input[0];
        int numberOfSticks = Integer.valueOf(input[1]);

        int weight = Integer.valueOf(input[2]);
        String copyNumber = input[3];
        Integer[] array = null;
        String fileName = "";
        if (fileName_input.contains("PR"))
            fileName = fileType + "_" + numberOfSticks + "_" + weight + "_" + copyNumber;
        else {
            fileName = fileType + "-" + numberOfSticks + "-" + weight + "-" + copyNumber;
        }
        File file = new File(ReadingPR.folder + fileName);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        if ((line = bufferedReader.readLine()) != null) {
            numberOfSticks = Integer.parseInt(line);
            array = new Integer[numberOfSticks];// [2];
            int index = 0;
            while (index < numberOfSticks && (line = bufferedReader.readLine()) != null) {
                String[] sT = line.split("\t");
                array[index] = Integer.parseInt(sT[1]);
                ++index;
            }
        }
        int maxWeight = 0;
        if ((line = bufferedReader.readLine()) != null) {
            maxWeight = Integer.parseInt(line);
        }

        ReadedData data = new ReadedData(numberOfSticks, array, maxWeight);

        return data;
    }
}
