package ca.inf4705.tp2.utils;

public class ObjectDoubleTableau {
    private int[] C = null;
    private int[] I = null;

    public ObjectDoubleTableau() {
        this.C = new int[10];
        this.I = new int[10];
    }

    public ObjectDoubleTableau(int[] C, int[] I) {
        this.C = C;
        this.I = I;
    }

    public int[] getC() {
        return this.C;
    }

    public int[] getI() {
        return this.I;
    }

    public void printC() {
        String output = "C : \n";
        for (int i = 0; i < this.C.length; ++i) {
            if (this.C[i] == Integer.MAX_VALUE) {
                output += "∞ ";
            } else
                output += this.C[i] + " ";
        }
        System.out.println(output);
    }

    public void printI() {
        String output = "I : \n";
        for (int i = 0; i < this.I.length; ++i) {
            if (this.I[i] == Integer.MAX_VALUE) {
                output += "∞ ";
            } else
                output += this.I[i] + " ";
        }
        System.out.println(output);
    }

    public void printCI() {
        String output = "C\tI \n";
        output += "-------\n";
        for (int i = 0; i < this.I.length; ++i) {
            if (this.I[i] == 0) {
                if (this.C[i] == 0) {
                    output += "0\t0" + "\n";
                } else {
                    output += "this.C[i]\t∞" + "\n";
                }
            } else {
                if (this.C[i] == 0) {
                    output += "0\t" + this.I[i] + "\n";
                } else {
                    output += this.C[i] + "\t" + this.I[i] + "\n";
                }
            }
        }
        System.out.println(output);
    }
}