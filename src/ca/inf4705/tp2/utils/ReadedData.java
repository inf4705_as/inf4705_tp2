package ca.inf4705.tp2.utils;

public class ReadedData {
    private int numberOfSticks = 0;
    private int maxWeight = 0;
    private Integer[] arrayOfSticks = null;

    public ReadedData(int numberOfSticks, Integer[] arrayOfSticks, int maxWeight) {
        this.arrayOfSticks = arrayOfSticks;
        this.maxWeight = maxWeight;
        this.numberOfSticks = numberOfSticks;
    }

    public int getNumberOfSticks() {
        return this.numberOfSticks;
    }

    public int getMaxWeight() {
        return this.maxWeight;
    }

    public Integer[] getArrayOfSticks() {
        return this.arrayOfSticks;
    }

    public String toString() {
        return "Lenght of array : " + this.arrayOfSticks.length + "\nmax weight : " + this.maxWeight
                + "\nnumber of stick : " + this.numberOfSticks;
    }
}