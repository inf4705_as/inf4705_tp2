import argparse, os, sys, subprocess

os.environ["ANT_OPTS"] = "-Xmx2048m"
subprocess.call(["ant", "-q", "-S", "clean"]);
subprocess.call(["ant", "-q", "-S","jar"])

parser = argparse.ArgumentParser(description='This program will run the second work for the cours INF4705')
parser.add_argument('-a', '--algo', type=str, nargs='*', choices=["glouton", "progdyn1", "progdyn2", "recuit"],
                    help='Chose the algorithme to run')
parser.add_argument('-e', '--exemplaire', nargs='*', help='Chose your file to consider. These path must be absolute!')
parser.add_argument('-p', '--print', action="count", help='Print all the results')
parser.add_argument('-t', '--temps', action="count", help='Print the time taken by the chosed algorithmed')
args = parser.parse_args()

glouton = "glouton"
progdyn1 = "progdyn1" 
progdyn2 = "progdyn2"
recuit = "recuit"

fichierPR = ["PR_100_400_", "PR_500_2000_", "PR_1000_4000_"]
IterationPR = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

fichierWC = [
    "WC-10-10-", "WC-10-100-", "WC-10-1000-",
    "WC-20-10-", "WC-20-100-", "WC-20-1000-",
    "WC-50-10-", "WC-50-100-", "WC-50-1000-",
    "WC-100-10-", "WC-100-100-", "WC-100-1000-",
    "WC-200-10-", "WC-200-100-", "WC-200-1000-",
    "WC-500-10-", "WC-500-100-", "WC-500-1000-",
    "WC-1000-10-", "WC-1000-100-", "WC-1000-1000-",
    "WC-2000-10-", "WC-2000-100-", "WC-2000-1000-"
    ]
IterationWC = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]

isE = args.exemplaire is not None
isP = args.print is not None
isT= args.temps is not None
e_path = ""


def iterFunc(algo):
    for fichier in fichierPR:
        for num in IterationPR:
            txt_fichier = str(fichier + num + ".txt")
            # print("txt_fichier : " + str(txt_fichier))
            subprocess.call(
                [
                    "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/"+ algo +".jar", 
                    str(isP), str(isE), str(txt_fichier), str(isT) 
                ]
            )
    for fichier in fichierWC:
        for num in IterationWC:
            txt_fichier = str(fichier + num + ".txt")
            # print("txt_fichier : " + str(txt_fichier))
            subprocess.call(
                [
                    "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/"+ algo +".jar", 
                    str(isP), str(isE), str(txt_fichier), str(isT) 
                ]
            )
    pass



if isE :
    if len(args.exemplaire) == 1:
        e_path = args.exemplaire[0]
    else:
        print("Option -e : Pas assez ou trop de path")

if args.algo is not None:
    # print("You have enter " + str(args.algo))
    if len(args.algo) == 0 :
        print("You have not chosen an algorithme.");
    elif len(args.algo) > 1 :
        print("Too much parameters have been entered for the option -a");
    elif str(args.algo[0]) == recuit:
        # print("You have choosen : " + recuit);
        if(not isE):
            iterFunc("Recuit")
        else:
            subprocess.call(
                    [
                        "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Recuit.jar", 
                        str(isP), str(isE), str(e_path), str(isT) 
                    ]
                )
        
    elif str(args.algo[0]) == glouton:
        # print("You have choosen : " + glouton);
        if(not isE):
            iterFunc("Glouton")
        else:
            subprocess.call(
                    [
                        "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Glouton.jar", 
                        str(isP), str(isE), str(e_path), str(isT) 
                    ]
                )
        
    elif str(args.algo[0]) == progdyn1:
        # print("You have choosen : " + progdyn1);
        if(not isE):
            iterFunc("DynamiqueEssai1")
        else:
            subprocess.call(
                    [
                        "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/DynamiqueEssai1.jar", 
                        str(isP), str(isE), str(e_path), str(isT) 
                    ]
                )
        
    elif str(args.algo[0]) == progdyn2:
        # print("You have choosen : " + progdyn2);
        if(not isE):
            iterFunc("DynamiqueEssai2")
        else:
            subprocess.call(
                    [
                        "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/DynamiqueEssai2.jar", 
                        str(isP), str(isE), str(e_path), str(isT) 
                    ]
                )
        

else:
    print("You have not entered the option -a or --algo")

subprocess.call(["ant", "-q", "-S", "clean"]);
